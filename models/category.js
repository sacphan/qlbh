const db=require('../controllers/db');
const tbName='Categories';

module.exports={
     all: async()=>{
        const sql=`SELECT * FROM ${tbName}`;
        const rows= db.load(sql);
        return rows;
    }
}