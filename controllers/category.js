const express=require('express');
const router=express.Router();
const mCat=require('../models/category');
const mPro=require('../models/product');

router.get('/',async(req,res)=>{
    const cats=await mCat.all();
   
    for (let cat of cats)
    {
        cat.isActive=false;
    }
    cats[0].isActive=true;
    res.render('partials/home',{
        title:'cat test',
        cats: cats
    })
  
})
router.get('/:id/products',async(req,res)=>{
   
    const id=req.params.id;
    const page=1;
   
    const cats=await mCat.all();
    const ps=await mPro.allByCatId(id,page);
    for (let cat of cats)
    {      
        if (cat.CatID==id) cat.isActive=true;
        else cat.isActive=false;    
    }
    // for (let pro of ps)
    // {
    //     pro.linkImage=`../public/sp/`
    // }
    res.render('partials/home',{
        title:'cat test',
        cats:cats,
        ps:ps
    });

})
router.get('/:id/:page/products',async(req,res)=>{
   
    const id=req.params.id;
    const page=req.params.page;
   
    const cats=await mCat.all();
    const ps=await mPro.allByCatId(id,page);
    for (let cat of cats)
    {      
        if (cat.CatID==id) cat.isActive=true;
        else cat.isActive=false;    
    }
    // for (let pro of ps)
    // {
    //     pro.linkImage=`../public/sp/`
    // }
    res.status(200).send({
        title:'cat test',
        cats:cats,
        ps:ps
    });

})
module.exports=router;
