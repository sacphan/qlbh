const express=require('express');
const router=express.Router();
const mCat=require('../models/category');
const mPro=require('../models/product');

router.get('/:id/detail',async(req,res)=>{
    const id=req.params.id;
    const ps= await mPro.getDetail(id);
  
    res.render('partials/DetailProduct',{title:'Detail Product',layout:'detailproduct',ps:ps});

});
module.exports=router;