var express =require ("express");
var app =express();
const exphps=require('express-handlebars');
const path = require("path");

//setup template engine(hbs)
const hbs=exphps.create({
    defaultLayout:'mains',
    extname:'hbs'
})
app.engine('hbs',hbs.engine);
app.set('view engine','hbs');

//static files
app.use(express.static('./public'));

//fire controllers
// home(app);
app.get('/',(req,res)=>{
    res.render('partials/home');    
})
 app.use('/cat',require('./controllers/category'));
 app.use('/Product',require('./controllers/product'));
 app.get('/:id/mainimage',(req,res)=>{
     const id=req.params.id;
  
     res.sendFile(path.join(__dirname,`./public/sp/${id}/main_thumbs.jpg`));
 })
 app.get('/:id/:nameImg/detailPanelImage',(req,res)=>{
     try{
        const id=req.params.id;
        const nameImg=req.params.nameImg;
        res.sendFile(path.join(__dirname,`./public/sp/${id}/${nameImg}`));
     }
     catch{
         res.sendFile(path.join(__dirname,`Failed`));
     }
   
})
 
//listen port 
app.listen(4444);
console.log("server running on port 4444");